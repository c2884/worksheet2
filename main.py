import morse

if __name__ == "__main__":
    e = morse.encode('US')
    print('%s' % e)
    d = morse.decode("... -.-. .--")
assert morse.encode('US') == '..- ... ', "Should be ..-"
assert morse.decode('..- ...') == 'US', "Should be ..-"