import unittest
import morse

class TestMorse(unittest.TestCase):
    def test_encode_us(self):
        self.assertEqual( morse.encode('US'), '..- ... ')
        self.assertEqual( morse.decode('...-..-'), '$')
        self.assertEqual( morse.encode('HELLO'), '.... . .-.. .-.. --- ')
        self.assertEqual( morse.decode('---...'), ':')

if __name__ == '__main__':
    unittest.main()