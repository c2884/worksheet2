import morse

def test_encode_us():
    assert morse.encode('US') == '..- ... ', "Should be ..- ..."
    assert morse.decode('...-..-') == '$', "Should be  $"

if __name__ == "__main__":
    test_encode_us()
    print('Everything passed')