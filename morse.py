class Tree_Node:
    # Tree constructor
    def __init__(self, root=None, left=None, right=None):
        self.root = root
        self.left_subtree = left
        self.right_subtree = right    

tree = Tree_Node("START") #The root node of Binary tree

# 1st Level of Binary Tree
tree.left_subtree = Tree_Node("E")
tree.right_subtree = Tree_Node("T")

# 2nd Level of Binary Tree
tree.left_subtree.left_subtree = Tree_Node("I")
tree.left_subtree.right_subtree = Tree_Node("A")
tree.right_subtree.left_subtree = Tree_Node("N")
tree.right_subtree.right_subtree = Tree_Node("M")

# 3rd Level of Binary Tree
tree.left_subtree.left_subtree.left_subtree = Tree_Node("S")
tree.left_subtree.left_subtree.right_subtree = Tree_Node("U")
tree.left_subtree.right_subtree.left_subtree = Tree_Node("R")
tree.left_subtree.right_subtree.right_subtree = Tree_Node("W")

tree.right_subtree.left_subtree.left_subtree = Tree_Node("D")
tree.right_subtree.left_subtree.right_subtree = Tree_Node("K")
tree.right_subtree.right_subtree.left_subtree = Tree_Node("G")
tree.right_subtree.right_subtree.right_subtree = Tree_Node("O")

# 4th Level of Binary Tree
tree.left_subtree.left_subtree.left_subtree.left_subtree = Tree_Node("H")
tree.left_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("V")
tree.left_subtree.left_subtree.right_subtree.left_subtree = Tree_Node("F")
tree.left_subtree.left_subtree.right_subtree.right_subtree = Tree_Node("")
tree.left_subtree.right_subtree.left_subtree.left_subtree = Tree_Node("L")
tree.left_subtree.right_subtree.left_subtree.right_subtree = Tree_Node("")
tree.left_subtree.right_subtree.right_subtree.left_subtree = Tree_Node("P")
tree.left_subtree.right_subtree.right_subtree.right_subtree = Tree_Node("J")

tree.right_subtree.left_subtree.left_subtree.left_subtree = Tree_Node("B")
tree.right_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("X")
tree.right_subtree.left_subtree.right_subtree.left_subtree = Tree_Node("C")
tree.right_subtree.left_subtree.right_subtree.right_subtree = Tree_Node("Y")
tree.right_subtree.right_subtree.left_subtree.left_subtree = Tree_Node("Z")
tree.right_subtree.right_subtree.left_subtree.right_subtree = Tree_Node("Q")
tree.right_subtree.right_subtree.right_subtree.left_subtree = Tree_Node("")
tree.right_subtree.right_subtree.right_subtree.right_subtree = Tree_Node("")

# 5th Level of Binary Tree
tree.left_subtree.left_subtree.right_subtree.left_subtree.right_subtree = Tree_Node("¿")
tree.left_subtree.left_subtree.right_subtree.right_subtree.left_subtree = Tree_Node("?")
tree.left_subtree.left_subtree.left_subtree.right_subtree.left_subtree = Tree_Node("")
tree.left_subtree.right_subtree.left_subtree.left_subtree.left_subtree = Tree_Node("&")
tree.left_subtree.right_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("")
tree.left_subtree.right_subtree.left_subtree.right_subtree.left_subtree = Tree_Node("+")
tree.left_subtree.right_subtree.right_subtree.right_subtree.right_subtree = Tree_Node("")

tree.right_subtree.left_subtree.right_subtree.right_subtree.left_subtree = Tree_Node("(")
tree.right_subtree.left_subtree.right_subtree.left_subtree.right_subtree = Tree_Node("")
tree.right_subtree.right_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("")
tree.right_subtree.right_subtree.left_subtree.left_subtree.left_subtree = Tree_Node("")
tree.right_subtree.left_subtree.left_subtree.left_subtree.left_subtree = Tree_Node("")
tree.right_subtree.right_subtree.right_subtree.left_subtree.left_subtree = Tree_Node("")

# 6th Level of Binary Tree
tree.left_subtree.left_subtree.left_subtree.right_subtree.left_subtree.left_subtree = Tree_Node("")
tree.left_subtree.left_subtree.right_subtree.right_subtree.left_subtree.right_subtree = Tree_Node("_")
tree.left_subtree.right_subtree.left_subtree.left_subtree.right_subtree.left_subtree = Tree_Node("\"")
tree.left_subtree.right_subtree.left_subtree.right_subtree.left_subtree.right_subtree = Tree_Node(".")
tree.left_subtree.right_subtree.right_subtree.right_subtree.right_subtree.left_subtree = Tree_Node("'")

tree.right_subtree.left_subtree.right_subtree.right_subtree.left_subtree.right_subtree = Tree_Node(")")
tree.right_subtree.left_subtree.right_subtree.left_subtree.right_subtree.left_subtree = Tree_Node(";")
tree.right_subtree.left_subtree.right_subtree.left_subtree.right_subtree.right_subtree = Tree_Node("!")
tree.right_subtree.left_subtree.left_subtree.left_subtree.left_subtree.right_subtree= Tree_Node("-")
tree.right_subtree.right_subtree.left_subtree.left_subtree.right_subtree.right_subtree = Tree_Node(",")
tree.right_subtree.right_subtree.left_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("¡")
tree.right_subtree.right_subtree.right_subtree.left_subtree.left_subtree.left_subtree = Tree_Node(":")

# 7th level of Binary Tree  
tree.left_subtree.left_subtree.left_subtree.right_subtree.left_subtree.left_subtree.right_subtree = Tree_Node("$")

def morse_code(node, element, lst):
    if node==None:
        return False
    elif node.root == element:
        return True
    else:
        if morse_code(node.left_subtree, element,lst) == True:
            lst.insert(0,".")
            return True
        elif morse_code(node.right_subtree, element,lst) == True:
            lst.insert(0,"-")
            return True
        
def retrieve_plainText(node, el,lst):
    if len(el) == 0:
        lst.append(node.root)
        return True
    elif el[:1] == ".":
        retrieve_plainText(node.left_subtree, el[1:], lst)
    elif el[:1] == "-":
        retrieve_plainText(node.right_subtree, el[1:], lst)         

def encode(message):
    m_code = ""
    for el in message:
        lst = []
        morse_code(tree, el, lst)
        encode = "".join(lst)
        m_code = m_code + encode + " "
    return m_code

def decode(message):
    characters = message.split(" ")
    decode_msg = ""
    lst = []
    char_len = len(characters)
    for morse_code in characters:
        retrieve_plainText(tree, morse_code, lst)
        decode = "".join(lst)
        decode_msg = decode_msg + decode
    return str(decode_msg[-char_len:])
    

